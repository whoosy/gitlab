# frozen_string_literal: true

require 'spec_helper'

# rubocop: disable Lint/EmptyBlock
# rubocop: disable RSpec/EmptyExampleGroup
RSpec.describe Organization, type: :model, feature_category: :cell do
end
# rubocop: enable RSpec/EmptyExampleGroup
# rubocop: enable Lint/EmptyBlock
